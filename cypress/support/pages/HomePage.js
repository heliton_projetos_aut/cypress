/// <reference types="cypress" />

//nessa linha de código eu estou importando o arquivo HomeElements que está na pasta elements
import HomeElements from '../elements/HomeElements'
const homeElements = new HomeElements


//variaveis

//dessa maneira que cria uma classe (classe= pacote de funções ou ações)
//os arquivos pages sempre vão consumir (pegar, ultilizar) os elementos (botão,campo,link,etc) dos arquivos elements
class HomePage{
  
    //dessa maneira que cria uma função (função = ações)
    acessarSite(){
        //dentro da função eu coloco os comandos cypress(visit,click,selector, etc.)
        cy.visit('https://www.fcc.com.br/')
    }
    clicarBtnTrabalheConosco(){
        cy.get(homeElements.btnTrabalheConosco()).wait(3000).click({force:true}) 
    }
    clicarBtnBanner(){
        cy.get(homeElements.btnFecharBanner()).wait(3000).click({force:true})
    }
    clicarBtnMenu(){
        cy.get(homeElements.btnMenu()).wait(3000).click({force:true})
    }

}
//essa linha de código eu estou exportando (deixando visível) as funções (ações, ex:acessar, clicar, escrever)
//por padrão os arquivos steps serão aqueles que irão usar essas ações.
export default HomePage;