/// <reference types="cypress" />


import TrabalheConoscoElements from '../elements/TrabalheConoscoElements'
const trabalheConoscoElements = new TrabalheConoscoElements

class TrabalheConoscoPage{

    selecionarAreaDeInteresse(){
        cy.get(trabalheConoscoElements.campoAreaDeInteresse()).select('Comercial')

    }
    preencherNome(){
        cy.get(trabalheConoscoElements.campoNome()).type('teste')
    }
    preencherEmail(){
        cy.get(trabalheConoscoElements.campoEmail()).type('teste@teste.com.br')
    }
    preencherTelefone(){
        cy.get(trabalheConoscoElements.campoTelefone()).type('998989595')
    }
    selecionarPais(){
        cy.get(trabalheConoscoElements.campoPaís()).select('Brasil',{force:true})
    }
    selecionarEstado(){
        cy.get(trabalheConoscoElements.campoEstado()).select('Amazonas',{force:true})
    }
    selecionarCidade(){
        cy.get(trabalheConoscoElements.campoCidade()).select('Manaus',{force:true})
    }
    escreverMensagem(){
        cy.get(trabalheConoscoElements.campoMensegem()).type('teste')
    }
    clicarBtnEnviarFormulario(){
        cy.get(trabalheConoscoElements.BtnEnviarFormulario()).click()
    }
    validarMensegemAgradecimento(){
        cy.get(trabalheConoscoElements.mensagemDeAgradecimento()).should('contain','Agradecemos a sua mensagem.')
    }


}
export default TrabalheConoscoPage;


