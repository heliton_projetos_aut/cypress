/// <reference types="cypress" />


import TrabalheConoscoPage from '../pages/TrabalheConoscoPage'

const trabalheConoscoPage = new TrabalheConoscoPage

And("preencho os campos obrigatórios do formulário", () => {
	trabalheConoscoPage.selecionarAreaDeInteresse();
	trabalheConoscoPage.preencherNome();
	trabalheConoscoPage.preencherEmail();
	trabalheConoscoPage.preencherTelefone();
	trabalheConoscoPage.selecionarPais();
	trabalheConoscoPage.selecionarEstado();
	trabalheConoscoPage.selecionarCidade();
	trabalheConoscoPage.escreverMensagem();
});

When("clico no botão enviar formulário", () => {
	trabalheConoscoPage.clicarBtnEnviarFormulario();
});

Then("devo receber a mensagem de envio com sucesso", () => {
	trabalheConoscoPage.validarMensegemAgradecimento();
});

	










