class TrabalheConoscoElements{
    campoAreaDeInteresse = () => {return '[class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required"]'}
    campoNome = () => {return '[class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"]'}
    campoEmail = () => {return '[class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"]'}
    campoTelefone = () => {return '[class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel"]'}
    campoPaís = () => {return 'select[id="pais_contato"]'}
    campoEstado = () => {return 'select[id="estado"]'}
    campoCidade = () => {return 'select[id="cidade"]'}
    campoMensegem = () => {return '[class="wpcf7-form-control wpcf7-textarea"]'}
    BtnEnviarFormulario = () => {return 'input[value="Enviar Formulário"]'}
    mensagemDeAgradecimento = () => {return '#wpcf7-f194-o1 > form > div.wpcf7-response-output.wpcf7-display-none.wpcf7-mail-sent-ok'}

}
export default TrabalheConoscoElements;