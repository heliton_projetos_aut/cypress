
//dessa maneira que cria uma classe para colocar os endereços dos elementos do site(ex:botão, campo, link, imagens, etc)
//os arquivos pages sempre vão consumir (pegar, ultilizar) os elementos (botão,campo,link,etc) dos arquivos elements

class HomeElements{

    //o primeiro nome na frente dessa linha de codigo abaixo é o nome que eu estou dando ao botão 
    //o endereço dos elementos por padrão deve ficar entre as aspas simples
    //sugestão:
    //nome do botão pode começar com btn
    //nome de campo pode começar como imput (ex:imputLogin, imputSenha, imputPesquisa, etc.)
    //nome de frases com link pode começar com link
    
    btnTrabalheConosco = () => {return 'header .main-menu-container nav ul li:nth-child(6) a ~ ul li ~ li a'}
    btnMenu = () => {return '#menu'}
    btnFecharBanner = () => {return 'body > div.wrap > div > main > div.modal-black-week > div.vheight > div > div.place-fechar > a > i'}



    

    


}
//essa linha de código eu estou exportando (deixando visível) os elementos (ex:botão, campo, link, imagens, etc)
//do siteque esta sendo mapeado
//por padrão os arquivos pages serão aqueles que irão usar esses elementos.
export default HomeElements;